describe('Telemetron Util Unit testing', function () {
    var telemetronUtil;

    beforeEach(function () {
        telemetronUtil = new TelemetronUtil({
            debug: true,
            apiAddress: '//beacon.telemetron.io',
            enabled: true,
            flushInterval: 30000
        });
    });

    it('should not send request', function () {
        spyOn(XMLHttpRequest.prototype, 'send');


        telemetronUtil.sendRequest('endpoint', 'GET2', '?id=1');

        expect(XMLHttpRequest.prototype.send.calls.count()).toEqual(0);
    });


    it('should to send GET request', function () {
        jasmine.Ajax.install();
        spyOn(console, 'log');

        telemetronUtil.sendRequest('endpoint', 'GET', '?id=1');

        expect(console.log).toHaveBeenCalled();

        var request = jasmine.Ajax.requests.mostRecent();

        expect(request.method).toBe('GET');
        expect(request.url).toBe('//beacon.telemetron.io/endpoint/?id=1');

        jasmine.Ajax.uninstall();
    });

    it('should to send GET request without logs', function () {
        jasmine.Ajax.install();
        spyOn(console, 'log');

        telemetronUtil = new TelemetronUtil({
            debug: false,
            apiAddress: '//beacon.telemetron.io',
            enabled: true
        });

        telemetronUtil.sendRequest('endpoint', 'GET', '?id=1');

        var request = jasmine.Ajax.requests.mostRecent();

        expect(request.method).toBe('GET');
        expect(request.url).toBe('//beacon.telemetron.io/endpoint/?id=1');

        expect(console.log.calls.count()).toEqual(0);

        jasmine.Ajax.uninstall();
    });

    it('should to send POST request', function () {
        jasmine.Ajax.install();
        spyOn(console, 'log');

        telemetronUtil.sendRequest('endpoint', 'POST', {
            id: 1
        });

        var request = jasmine.Ajax.requests.mostRecent();

        request.respondWith({
            status: 200,
            responseText: ''
        });

        expect(request.method).toBe('POST');
        expect(request.url).toBe('//beacon.telemetron.io/endpoint');

        jasmine.Ajax.uninstall();
    });

    it('should to send POST request without logs', function () {
        jasmine.Ajax.install();
        spyOn(console, 'log');

        telemetronUtil = new TelemetronUtil({
            debug: false,
            apiAddress: '//beacon.telemetron.io',
            enabled: true
        });

        telemetronUtil.sendRequest('endpoint', 'POST', {
            id: 1
        });

        var request = jasmine.Ajax.requests.mostRecent();

        request.respondWith({
            status: 200,
            responseText: ''
        });

        expect(request.method).toBe('POST');
        expect(request.url).toBe('//beacon.telemetron.io/endpoint');

        expect(console.log.calls.count()).toEqual(0);

        jasmine.Ajax.uninstall();
    });

    it("should register a new Queue and send data", function () {
        jasmine.clock().install();
        spyOn(telemetronUtil, 'sendRequest');

        expect(telemetronUtil.registerQueue('metrics', 'endpoint', 5000)).toEqual(true);

        telemetronUtil.addItemToQueue('metrics', {
            name: 'test',
            type: 'counter',
            value: 1
        });
        jasmine.clock().tick(5001);

        expect(telemetronUtil.sendRequest).toHaveBeenCalledWith('endpoint', 'POST', '[{"name":"test","type":"counter","value":1}]');

        jasmine.clock().uninstall();
    });

    it("should register a new Queue without interval expect default timer 30000", function () {
        expect(telemetronUtil.registerQueue('metrics', 'endpoint')).toEqual(true);
    });

    it("should not register a new Queue invalid inputs", function () {
        expect(telemetronUtil.registerQueue([], 'endpoint')).toEqual(false);
    });

    it("should register a new Queue and not send data", function () {
        jasmine.clock().install();

        telemetronUtil = new TelemetronUtil({
            debug: false,
            apiAddress: '//beacon.telemetron.io',
            dryrun: true
        });

        spyOn(telemetronUtil, 'sendRequest');

        telemetronUtil.registerQueue('metrics', 'endpoint', 5000);

        telemetronUtil.addItemToQueue('metrics', {
            name: 'test',
            type: 'counter',
            value: 1
        });
        jasmine.clock().tick(5001);

        expect(telemetronUtil.sendRequest.calls.count()).toEqual(0);

        jasmine.clock().uninstall();
    });

    it("should unregister queue", function () {
        jasmine.clock().install();
        spyOn(telemetronUtil, 'sendRequest');

        telemetronUtil.registerQueue('metrics', 'endpoint', 5000);

        jasmine.clock().tick(5001);

        expect(telemetronUtil.sendRequest.calls.count()).toEqual(0);

        telemetronUtil.addItemToQueue('metrics', {
            name: 'test',
            type: 'counter',
            value: 1
        });

        telemetronUtil.unregisterQueue('metrics');

        jasmine.clock().tick(10001);

        expect(telemetronUtil.sendRequest.calls.count()).toEqual(0);

        jasmine.clock().uninstall();
    });


    it('should not unregister queue', function () {
        expect(telemetronUtil.unregisterQueue('metrics')).toEqual(false);
    });

    it('should not add item to queue', function () {
        expect(telemetronUtil.addItemToQueue('metrics', {
            name: 'test',
            type: 'counter',
            value: 1
        })).toEqual(false);
    });

});
