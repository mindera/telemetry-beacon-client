describe('Exception module tests', function () {
    var exceptionHandlerProvider;
    var $rootScope;
    var exceptionHandler;
    var mocks = {
        errorMessage: 'fake error'
    };

    beforeEach(function() {
        module('telemetron.beacon.client.exceptions', function(_exceptionHandlerProvider_) {
            exceptionHandlerProvider = _exceptionHandlerProvider_;
        });
    });

    beforeEach(inject(function(_$rootScope_, _exceptionHandler_) {
        $rootScope = _$rootScope_;
        exceptionHandler = _exceptionHandler_;
    }));

    describe('Exception Handler tests', function() {
        it('should have a dummy test', function() {
            expect(true).toBe(true);
        });

        it('should be defined', function() {
            expect(exceptionHandler).toBeDefined();
        });

        it('should have configuration', function() {
            expect(exceptionHandler.config).toBeDefined();
        });
    });

    function functionThatWillThrow() {
        throw new Error(mocks.errorMessage);
    }
});
