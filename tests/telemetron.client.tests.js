describe('Telemetron Client Unit testing', function () {
    'use strict';

    afterEach(function () {
        telemetron.perf.clearMarks();
        telemetron.perf.clearMeasures();
    });

    it('should exist the module telemetron and have configs', function () {
        expect(telemetron).not.toBeNull();
        expect(telemetron.apiAddress).toEqual('//beacon.telemetron.io');
        expect(telemetron.endpoints.metrics).toEqual('beacon/metrics');
        expect(telemetron.firstClickFired).toEqual(false);

    });

    it('should should have defaults', function () {
        expect(telemetron.defaults).not.toBeNull();
        expect(telemetron.defaults.dryrun).toEqual(false);
        expect(telemetron.defaults.debug).toEqual(false);
        expect(telemetron.defaults.environment).toEqual(undefined);
        expect(telemetron.defaults.registerFirstClick).toEqual(true);
        expect(telemetron.defaults.firstClickMeasureName).toEqual('firstclick');
        expect(telemetron.defaults.firstClickMetricName).toEqual('timeto');
        expect(telemetron.defaults.firstClickMetricTags).toEqual({mark:'firstclick'});
        expect(telemetron.defaults.appLoadedMeasureName).toEqual('apploaded');
        expect(telemetron.defaults.appLoadedMetricName).toEqual('timeto');
        expect(telemetron.defaults.appLoadedMetricTags).toEqual({mark:'apploaded'});
        expect(telemetron.defaults.tags).toEqual({});
        expect(telemetron.defaults.timer.tags).toEqual({});
        expect(telemetron.defaults.counter.tags).toEqual({});
        expect(telemetron.defaults.gauge.tags).toEqual({});
        expect(telemetron.defaults.other.tags).toEqual({});
        expect(telemetron.defaults.aggregations).toEqual([]);
        expect(telemetron.defaults.timer.aggregations).toEqual(['avg', 'p90', 'count']);
        expect(telemetron.defaults.counter.aggregations).toEqual(['sum', 'count']);
        expect(telemetron.defaults.gauge.aggregations).toEqual(['sum', 'count']);
        expect(telemetron.defaults.other.aggregations).toEqual(['last']);
        expect(telemetron.defaults.aggregationFrequency).toEqual(10);
        expect(telemetron.defaults.flushInterval).toEqual(30000);
    });

    it('should merge global, method, type tags and aggregations and override aggregation frequency', function() {
        telemetron.initialize({
            tags: {},
            aggregations: ['last'],
            timer: {tags: {foo: 'bar'}},
            gauge: {tags: {meh: 'yep'}, aggregations: ['derivative']}
        });

        var util = telemetron.util;

        spyOn(util, 'addItemToQueue');

        var options = {
            tags: {mark: 'gauge'},
            aggregations: [],
            aggregationFrequency: 300
        };

        telemetron.registerGauge('test', 1234, options);

        expect(util.addItemToQueue).toHaveBeenCalledWith('metrics', {
            name: 'test',
            type: 'gauge',
            value:  1234,
            tags: {mark: 'gauge', meh: 'yep'},
            aggregations: ['last', 'derivative'],
            aggregationFrequency: 300,
            namespace: 'web'
        });
    });

    it('should override aggregation frequency by type over global', function() {
        telemetron.initialize({
            tags: {},
            aggregations: ['last'],
            timer: {tags: {foo: 'bar'}},
            gauge: {tags: {meh: 'yep'}, aggregations: ['derivative'], aggregationFrequency: 60}
        });

        var util = telemetron.util;

        spyOn(util, 'addItemToQueue');

        var options = {
            tags: {mark: 'gauge'},
            aggregations: []
        };

        telemetron.registerGauge('test', 1234, options);

        expect(util.addItemToQueue).toHaveBeenCalledWith('metrics', {
            name: 'test',
            type: 'gauge',
            value:  1234,
            tags: {mark: 'gauge', meh: 'yep'},
            aggregations: ['last', 'derivative'],
            aggregationFrequency: 60,
            namespace: 'web'
        });
    });

    it('should discard invalid aggregations', function() {
        telemetron.initialize({
            tags: {},
            aggregations: ['last', 'invalid'],
            timer: {tags: {foo: 'bar'}},
            gauge: {tags: {meh: 'yep'}, aggregations: ['derivative']}
        });

        var util = telemetron.util;

        spyOn(util, 'addItemToQueue');

        var options = {
            tags: {mark: 'gauge'},
            aggregations: ['fail'],
            aggregationFrequency: 300
        };

        telemetron.registerGauge('test', 1234, options);

        expect(util.addItemToQueue).toHaveBeenCalledWith('metrics', {
            name: 'test',
            type: 'gauge',
            value:  1234,
            tags: {mark: 'gauge', meh: 'yep'},
            aggregations: ['last', 'derivative'],
            aggregationFrequency: 300,
            namespace: 'web'
        });
    });

    it('should discard invalid aggregation frequency', function() {
        telemetron.initialize({
            tags: {},
            aggregations: [],
            gauge: {tags: {meh: 'yep'}, aggregations: []}
        });

        var util = telemetron.util;

        spyOn(util, 'addItemToQueue');

        var options = {
            tags: {mark: 'gauge'},
            aggregationFrequency: 1234
        };

        telemetron.registerGauge('test', 1234, options);

        expect(util.addItemToQueue).toHaveBeenCalledWith('metrics', {
            name: 'test',
            type: 'gauge',
            value:  1234,
            tags: {mark: 'gauge', meh: 'yep'},
            aggregations: [],
            aggregationFrequency: 10,
            namespace: 'web'
        });
    });

    it('should register event for first click', function () {
        spyOn(telemetron, 'firstClick');
        telemetron.initialize();
        expect(telemetron.firstClick).toHaveBeenCalled();
    });

    it('should have an instance of TelemetronUtil', function() {
        telemetron.initialize();
        var utilInstance = telemetron.util instanceof TelemetronUtil;
        expect(utilInstance).toEqual(true);
    });

    it('should not register event for first click', function () {
        spyOn(telemetron, 'firstClick');

        telemetron.initialize({
            registerFirstClick: false
        });

        expect(telemetron.firstClick.calls.count()).toEqual(0);
    });

    it('should handle events', function (){
        spyOn(telemetron, 'handleFirstClick');
        telemetron.initialize();

        telemetron.handleEvent({
            type: 'click'
        });

        expect(telemetron.handleFirstClick).toHaveBeenCalled();
    });

    it('should registerMeasure on first click', function (){
        spyOn(telemetron, 'registerMeasure');
        telemetron.initialize();

        telemetron.handleFirstClick();

        expect(telemetron.registerMeasure).toHaveBeenCalledWith('firstclick', 'timeto', {
            tags: {mark: 'firstclick'},
            aggregations: []
        });

        expect(telemetron.firstClickFired).toEqual(true);

        telemetron.handleFirstClick();

        expect(telemetron.registerMeasure.calls.count()).toEqual(1);

        telemetron.firstClickFired = false;
    });

    it('should override default config', function () {
        telemetron.initialize({
            enabled: false,
            firstClickMetricName: 'test.timeto.firstclick'
        });

        expect(telemetron.enabled).toEqual(false);
        expect(telemetron.firstClickMetricName).toEqual('test.timeto.firstclick');
    });

    it('should return the duration of a measure when calling measureTimeUserTiming', function () {
        telemetron.initialize();

        telemetron.registerMark('start_test');
        telemetron.registerMark('end_test');

        var options = {
            startMark: 'start_test',
            endMark: 'end_test',
            clearMarks: false,
            clearMeasures: false
        };
        telemetron.registerMeasure('measure', 'metric', options);

        expect(telemetron.measureTimeUserTiming('measure')).toEqual(jasmine.any(Number));

        telemetron.clearMarks();
        telemetron.clearMeasures();
    });

    it('should clear all performance marks when calling clearMarks without arguments', function () {
        telemetron.initialize();

        telemetron.registerMark('start_test');

        spyOn(telemetron.perf, 'clearMarks');

        telemetron.clearMarks();

        expect(telemetron.perf.clearMarks).toHaveBeenCalled();
    });

    it('should clear performance marks when calling clearMarks with an Array of marks', function () {
        telemetron.initialize();

        telemetron.registerMark('start_test');

        spyOn(telemetron.perf, 'clearMarks');

        telemetron.clearMarks(['start_test']);

        expect(telemetron.perf.clearMarks).toHaveBeenCalledWith('start_test');
    });

    it('should clear all performance measures when calling clearMeasures without arguments', function () {
        telemetron.initialize();

        spyOn(telemetron.perf, 'clearMeasures');

        telemetron.clearMeasures();

        expect(telemetron.perf.clearMeasures).toHaveBeenCalled();
    });

    it('should clear performance measures when calling clearMeasures with an Array of measures', function () {
        telemetron.initialize();

        telemetron.registerMeasure('measure', 'metric');

        spyOn(telemetron.perf, 'clearMarks');

        telemetron.clearMarks(['measure']);

        expect(telemetron.perf.clearMarks).toHaveBeenCalledWith('measure');
    });

    it('should add a performance mark when calling registerMark', function () {
        telemetron.initialize();

        spyOn(telemetron.perf, 'mark');

        telemetron.registerMark('mark_test');

        expect(telemetron.perf.mark).toHaveBeenCalledWith('mark_test');
    });

    it('should add a performance measure when calling registerMeasure', function () {
        telemetron.initialize();

        spyOn(telemetron.perf, 'measure');

        telemetron.registerMeasure('measure_test', 'metric_test');

        expect(telemetron.perf.measure).toHaveBeenCalledWith('measure_test', undefined, jasmine.any(String));
    });

    it('should call registerMeasure when calling appLoaded', function () {
        spyOn(telemetron, 'registerMeasure');

        telemetron.initialize({
            appLoadedMeasureName: 'measure_test',
            appLoadedMetricName: 'metric_test',
            appLoadedMetricTags: {mark: 'apploaded'}
        });

        telemetron.appLoaded();

        expect(telemetron.registerMeasure).toHaveBeenCalledWith(
            'measure_test',
            'metric_test',
            {
                tags: {mark: 'apploaded'},
                aggregations: []
            }
        );
    });

    it('should call addItemToQueue when registerMeasure', function() {
        telemetron.initialize();

        telemetron.registerMark('start_test');

        setTimeout(function() {
            telemetron.registerMark('end_test');

            var util = telemetron.util;
            spyOn(util, 'addItemToQueue');

            var options = {
                startMark: 'start_test',
                endMark: 'end_test',
                tags: {mark: 'measure'},
                aggregations: [],
                clearMarks: true,
                clearMeasures: true
            };
            telemetron.registerMeasure('measure_test', 'metric_test', options);

            expect(util.addItemToQueue).toHaveBeenCalled();
        }, 50);
    });

    it('should call addItemToQueue when registerMeasure with valid metric and default tags/aggregations', function() {
        telemetron.initialize({
            environment: 'production'
        });

        var util = telemetron.util;

        spyOn(util, 'addItemToQueue');

        var options = {
            tags: {mark: 'measure'},
            aggregations: []
        };

        telemetron.registerMeasure('measure_test', 'metric_test', options);

        expect(util.addItemToQueue).toHaveBeenCalledWith('metrics', {
            name: 'metric_test',
            type: 'timer',
            value: jasmine.any(Number),
            tags: {mark: 'measure', env: 'production'},
            aggregations: ['avg', 'p90', 'count'],
            aggregationFrequency: 10,
            namespace: 'web'
        });
    });

    it('should call addItemToQueue when registerTimer', function() {
        telemetron.initialize();

        var util = telemetron.util;

        spyOn(util, 'addItemToQueue');

        var options = {
            tags: {mark: 'foo'},
            aggregations: []
        };

        telemetron.registerTimer('load', 1234, options);

        expect(util.addItemToQueue).toHaveBeenCalled();
    });

    it('should not call addItemToQueue when invalid registerTimer', function() {
        telemetron.initialize();

        var util = telemetron.util;

        spyOn(util, 'addItemToQueue');

        telemetron.registerTimer();

        expect(util.addItemToQueue.calls.count()).toEqual(0);
    });


    it('should call addItemToQueue when registerCounter', function() {
        telemetron.initialize();

        var util = telemetron.util;

        spyOn(util, 'addItemToQueue');

        var options = {
            metricValue: 1,
            tags: {mark: 'foo'},
            aggregations: []
        };

        telemetron.registerCounter('load', options);

        expect(util.addItemToQueue).toHaveBeenCalled();
    });

    it('should call addItemToQueue when registerCounter without value', function() {
        telemetron.initialize();

        var util = telemetron.util;

        spyOn(util, 'addItemToQueue');

        telemetron.registerCounter('load');

        expect(util.addItemToQueue).toHaveBeenCalled();
    });

    it('should not call addItemToQueue when invalid registerCounter', function() {
        telemetron.initialize();

        var util = telemetron.util;

        spyOn(util, 'addItemToQueue');

        telemetron.registerCounter();

        expect(util.addItemToQueue.calls.count()).toEqual(0);
    });

    it('should call addItemToQueue when registerGauge with valid metric and default tags/aggregations', function() {
        telemetron.initialize({
            environment: 'production'
        });

        var util = telemetron.util;

        spyOn(util, 'addItemToQueue');

        var options = {
            tags: {mark: 'gauge'},
            aggregations: [],
            aggregationFrequency: 30
        };

        telemetron.registerGauge('test', 1234, options);

        expect(util.addItemToQueue).toHaveBeenCalledWith('metrics', {
            name: 'test',
            type: 'gauge',
            value:  1234,
            tags: {mark: 'gauge', env: 'production'},
            aggregations: ['sum', 'count'],
            aggregationFrequency: 30,
            namespace: 'web'
        });
    });

    it('should have logger enabled with options.debug as true', function () {
        telemetron.initialize({
            debug: true
        });

        expect(telemetron.log.enabled).toBeTruthy();
    });

    it('should have dryrun enabled with options.dryrun as true', function () {
        telemetron.initialize({
            dryrun: true
        });

        expect(telemetron.dryrun).toBeTruthy();
    });
});
