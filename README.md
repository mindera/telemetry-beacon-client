# README

This javascript file provides a client for the Telemetron service.

## How do I get set up?

```
#!html
<script type="text/javascript" src="js/telemetron.client.min.js"></script>
```

## Timings

All methods that provider timing mechanism are based in the [user-timing](http://www.w3.org/TR/user-timing/) specification.

### Configuration parameters

```
 telemetron.initialize({
   dryrun: false,
   debug: false,
   environment: 'local',
   namespace: 'web',
   app: 'my_app_name',
   registerFirstClick : true,
   firstClickMeasureName: 'firstclick',
   firstClickMetricName: 'timeto',
   firstClickMetricTags: {mark: 'firstclick'},
   appLoadedMeasureName: 'apploaded',
   appLoadedMetricName: 'timeto',
   appLoadedMetricTags: {mark: 'appLoaded'}
 });

```

### Register metrics

#### Timers

```
// Register timer with custom tag
telemetron.registerTimer('your.timer.metric.name', 100, {
    tags: {tagKey: 'foo'}
});

```

#### Counters

```
// Register counters

// Increment 1 (default)
telemetron.registerCounter('your.counter.metric.name');

// Increment 5
telemetron.registerCounter('your.counter.metric.name', {metricValue: 5});

// Increment 5 with custom tag and aggregation
telemetron.registerCounter('your.counter.metric.name', {
    metricValue: 5,
    tags: {tagKey: 'foo'},
    aggregations: ['avg']
});

```

#### Gauges

```
// Register a gauge with custom tag, aggregations and aggregation frequency
telemetron.registerGauge('your.gauge.metric.name', 345, {
    tags: {tagKey: 'foo'},
    aggregations: ['avg', 'last'],
    aggregationFrequency: 30
});

```


###  Pre-defined metrics

####  If you want to send a metric when your app is loaded, on document.ready per example, simply call:

```
telemetron.appLoaded();
```

### User Timing

Support for the [user-timing](http://www.w3.org/TR/user-timing/) specification is available.

#### Performance Mark

```
telemetron.registerMark('mark_start');
telemetron.registerMark('mark_end');
```

#### Performance Measure sent to Telemetron as a Timer

```
// Measure and Timer between two marks

telemetron.registerMark('mark_start');
telemetron.registerMark('mark_end');

var options = {
    startMark: 'mark_start',
    endMark: 'mark_end'
    tags: {mark: my_tag}
}

telemetron.registerMeasure('measure_name', 'metric_name' options);
```

```
// Measure and Timer from the navigationStart eent until the current time

var options = {
    tags: {mark: my_tag}
}

telemetron.registerMeasure('measure_name', 'metric_name' options);
```

You can omit both start and end mark names:

* if startMark is missing it will be measured from the navigationStart event
* if endMark is missing it will be measured until the current high precision time

### Resource Timing

Support for the [resource-timing](https://www.w3.org/TR/resource-timing/) specification is available and controlled as:

```
 telemetron.initialize({
    // enable resource loading tracking
    registerResourceLoading: true,
    // resource loading tracking interval
    resourceLoadingTrackingInterval: 5000,
    // exclude certain resource from being tracked
    resourceLoadingTrackingExclusions: [],
    // track resource names
    resourceLoadingNameTracking: {}
    ...
 });
```

Please note that not all browsers support the resource timing specification and therefore don't support resource tracking.

#### Configuration

You can enable or disable resource loading tracking by setting the `registerResourceLoading` attribute accordingly.

A blacklist of resource types can be defined to limit the type of resources tracked by setting the `resourceLoadingTypeBlacklist` attribute. 
This array should contain define types according to the resource timing specification which at the time are: link, css, script, img, object, subdocument, preflight, xmlhttprequest, svg, other.

You can apply a global resource filtering function based on the resource name by setting the `resourceLoadingPathFilter` attribute. This should be a callback function that receives the resource name and should return true or false to either include or exclude it.

If you want to capture the name of the resources being tracked as a tag you should use the `resourceLoadingNameTracking` attribute.

The way this works is that yu create an attribute in that object matching the initiatorType you want to track the name of and assign it a callback function which returns the parsed.

Example: Let's say you wanted to:

* track the script names
* remove the revisions of your bundle
* exclude the external jquery request
* exclude all img tags

```
// original resource name is foo.1234asdf.js
// parsed resource name is foo.js
var scriptCaptureFunc = function (name) {
    var captured = /.+\/(\w+)\.\w+\.(\w+)$/.exec(name);
    return captured[1] + '.' + captured[2];
};

// filter everything that's not jquery
var srcFilterFunc = function (name) {
   return name !== 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js';
};

telemetron.initialize({
    // enable resource loading tracking
    registerResourceLoading: true,
    // resource loading tracking interval
    resourceLoadingTrackingInterval: 5000,
    // resource type blacklist
    resourceLoadingTypeBlacklist: ['img'],
    // resource path whitelist
    resourceLoadingPathFilter: srcFilterFunc,
    // track resource names
    resourceLoadingNameTracking: {
      script: scriptCaptureFunc
    }
    ...
});
```
#### Cross Origin Requests

To being able to fully track asset loaded using cross origin requests please follow the [specification](https://www.w3.org/TR/resource-timing/#cross-origin-resources).

### Resource Loading Error

Support for the resource loading error specification is available and controlled as:

```
 telemetron.initialize({
    // enable resource error tracking
    registerResourceErrors: false,
    // track resource names on resource error tracking
    resourceErrorsNameTracking: {},
    // resource error type blacklist
    resourceErrorsTypeBlacklist: []
    ...
 });
```

IMPORTANT: If you want to track errors in every added resource you have to include and configure beacon client, before all other things, in your header.

That is controlled by a metric with name: '<namespace>'.counter.resource.error.

Please note that not all browsers support our resource error track implementation. The support depends on support for 'EventTarget.addEventListener' that you can check here: http://caniuse.com/#feat=addeventlistener

#### Configuration

You can enable or disable resources loading errors tracking by setting the `registerResourceErrors` attribute accordingly.

A blacklist of resource types can be defined to limit the type of resources tracked by setting the `resourceErrorsTypeBlacklist` attribute. 
This array should use the following available resource types: img, script, link.

If you want to capture a custom name of the resources being tracked as a tag, instead of default behaviour that captures all resource path as a tag (including http://), you should use the `resourceErrorsNameTracking` attribute. The way this works is that you create an attribute in that object matching the resource type you want to track the name of and assign it a callback function which returns the parsed.

Example: Let's say you wanted to:
- track the resources loading errors
- remove the revisions of your bundle
- exclude all img resources

```
// original resource name is foo.1234asdf.js
// parsed resource name is foo.js
var scriptCaptureFunc = function (name) {
    var captured = /.+\/(\w+)\.\w+\.(\w+)$/.exec(name);
    return captured[1] + '.' + captured[2];
};

telemetron.initialize({
    // enable resource error tracking
    registerResourceErrors: true,
    // track resource names on resource error tracking
    resourceErrorsNameTracking: {
      script: scriptCaptureFunc
    },
    // resource error type blacklist
    resourceErrorsTypeBlacklist: ['img']
    ...
});
```

### Error Tracking

The error tracking feature take care of app raised exceptions and register them as erros in a metric with name: '<namespace>'.counter.error being the tag the relative url where the error occurs (without any query parameter).

#### AngularJS Exception Handler Usage

Note: It is assumed that that telemetron has been already included in the desired app as is (However it's a better idea wrap whole telemetron in an angular provider).

1 - Include 'telemetron.exceptions.angular.js' file located in dist folder of bower component as a script on desired web app.

2 - Include 'telemetron.exceptions.angular' module on desired angular app.

3 - In angular app configuration file include telemetronExceptionHandlerProvider and configure it providing, as argument, telemetron client like in the following example: 

```
angularJsApp.config(configure);

configure.$inject = ['telemetronExceptionHandlerProvider'];
/* @ngInject */
function configure(telemetronExceptionHandlerProvider) {
    telemetronExceptionHandlerProvider.configure(telemetron);
}
```    



### Use as a bower component

```
bower install --save https://{username}@bitbucket.org/mindera/telemetry-beacon-client.git#{version}
```

### Development

```
// install deps
$ npm install

// run watch
$ grunt dev

// build dist
$ grunt
```