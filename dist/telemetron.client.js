/**
* Telemetron-beacon-client 5.5.0
* Copyright 2015 Mindera <http://www.mindera.com/>
*
*/
(function(window) {
    "use strict";
    var logger = {
        enabled: false,
        initialize: function(toggle) {
            if (toggle) {
                this.enabled = true;
                // istanbul ignore next
                this.debug = function(msg) {
                    console.log(msg);
                };
            }
        },
        debug: function(msg) {}
    };
    /**
     * Dummy default resource filter
     * @param name
     * @returns {boolean}
     */
    var defaultFilter = function(name) {
        return true;
    };
    /**
     * @description
     *
     * This object provides a client for the Telemetron service to register
     * application metrics. It can be called as follows:
     *
     * telemetron.initialize({
     *  debug: false,
     *  environment: 'local',
     *  namespace: 'mobile'
     *  registerFirstClick : true,
     *  firstClickMetricName: 'timeto',
     *  firstClickMetricTags: {mark: 'firstclick'},
     *  });
     * telemetron.registerMeasure('your.measure.name', 'your.metric.name');
     *
     */
    var telemetron = {
        // telemetron details
        apiAddress: "//beacon.telemetron.io",
        endpoints: {
            metrics: "beacon/metrics"
        },
        // console log accessor
        log: logger,
        // firstClick event helper
        firstClickFired: false,
        // window performance api accessor
        perf: window.performance,
        defaults: {
            // enable dryrun mode - ie. do not actually flush metrics out
            dryrun: false,
            // enable debug log messages
            debug: false,
            // environment to be used as tag - ie. development, production, etc.
            environment: undefined,
            // namespace to be used as metrics prefix - ie web, application, mobile, etc.
            namespace: "web",
            // global tags
            tags: {},
            // global aggregations
            aggregations: [],
            // aggregation frequency
            aggregationFrequency: 10,
            // metric type defaults
            timer: {
                tags: {},
                aggregations: [ "avg", "p90", "count" ]
            },
            counter: {
                tags: {},
                aggregations: [ "sum", "count" ]
            },
            gauge: {
                tags: {},
                aggregations: [ "sum", "count" ]
            },
            other: {
                tags: {},
                aggregations: [ "last" ],
                aggregationFrequency: 10
            },
            // enable first click tracking
            registerFirstClick: true,
            // enable resource error tracking
            registerResourceErrors: false,
            // track resource names on resource error tracking
            resourceErrorsNameTracking: {},
            // resource error type blacklist
            resourceErrorsTypeBlacklist: [],
            // enable resource loading tracking
            registerResourceLoading: true,
            // resource loading tracking interval
            resourceLoadingTrackingInterval: 5e3,
            // resource type blacklist
            resourceLoadingTypeBlacklist: [],
            // resource path whitelist
            resourceLoadingPathFilter: defaultFilter,
            // track resource names
            resourceLoadingNameTracking: {},
            firstClickMeasureName: "firstclick",
            firstClickMetricName: "timeto",
            firstClickMetricTags: {
                mark: "firstclick"
            },
            appLoadedMeasureName: "apploaded",
            appLoadedMetricName: "timeto",
            appLoadedMetricTags: {
                mark: "apploaded"
            },
            // metrics flush interval to telemetron
            flushInterval: 3e4
        },
        /**
         * Initialize the Telemetron client settings and register events
         */
        initialize: function(settings) {
            var self = this;
            this.defaults.debug = settings && settings.debug ? settings.debug : this.defaults.debug;
            this.log.initialize(this.defaults.debug);
            try {
                settings = typeof settings !== "undefined" ? settings : null;
                // Set default properties
                for (var key in self.defaults) {
                    if (self.defaults.hasOwnProperty(key)) {
                        self[key] = self.defaults[key];
                    }
                }
                // Set custom properties
                if (settings) {
                    for (key in settings) {
                        if (settings.hasOwnProperty(key)) {
                            self[key] = settings[key];
                        }
                    }
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
            // Set first click event
            if (self.registerFirstClick) {
                self.firstClick();
            }
            self.util = new TelemetronUtil({
                debug: this.debug,
                apiAddress: this.apiAddress,
                dryrun: this.dryrun,
                flushInterval: this.flushInterval
            });
            self.util.registerQueue("metrics", this.endpoints.metrics, this.flushInterval);
            if (self.registerResourceErrors) {
                self.trackResourceErrors();
            }
            if (self.registerResourceLoading && self.util.isResourceTimingSupported()) {
                self.trackResourceLoading();
            }
            // Metrics data object
            self.metricsData = function(name, type, value, tags, aggregations, aggregationFrequency) {
                return {
                    name: name,
                    type: type,
                    value: value,
                    tags: self.util.setTags(tags, self.tags, self[type].tags, self.environment, self.app),
                    aggregations: self.util.setAggregations(aggregations, self.aggregations, self[type].aggregations, self.log),
                    aggregationFrequency: self.util.setAggregationFrequency(aggregationFrequency, self.aggregationFrequency, self[type].aggregationFrequency, self.log),
                    namespace: self.namespace
                };
            };
        },
        /**
         * Register metric/mark when app is loaded
         */
        appLoaded: function() {
            this.registerMeasure(this.appLoadedMeasureName, this.appLoadedMetricName, {
                tags: this.appLoadedMetricTags,
                aggregations: []
            });
        },
        /**
         * Adds event listener to window on click
         * to register metric for first click
         */
        firstClick: function() {
            // istanbul ignore else
            if (window.addEventListener) {
                window.addEventListener("click", this);
            } else {
                this.log.debug("Click listener was not registered with success.");
            }
        },
        /**
         * Generic handler for events
         */
        handleEvent: function(event) {
            switch (event.type) {
              case "click":
                this.handleFirstClick();
                break;
            }
        },
        /**
         * Handle first click
         */
        handleFirstClick: function() {
            if (!this.firstClickFired) {
                this.registerMeasure(this.firstClickMeasureName, this.firstClickMetricName, {
                    tags: this.firstClickMetricTags,
                    aggregations: []
                });
                this.firstClickFired = true;
            }
        },
        /**
         * Track resource load error metrics
         */
        trackResourceErrors: function() {
            var self = this;
            // Apply user defined black list filter on resources to track errors
            var allElementsToCatch = [ "IMG", "SCRIPT", "LINK" ];
            var resourceErrorsTypeBlacklistUpperCase = self.resourceErrorsTypeBlacklist.map(function(value) {
                return value.toUpperCase();
            });
            var resourceEntriesToTrackErrors = allElementsToCatch.filter(function filter(entry) {
                // Exclude blacklisted types
                return resourceErrorsTypeBlacklistUpperCase.indexOf(entry) === -1;
            });
            window.addEventListener("error", function(event) {
                var element = event.target;
                if (resourceEntriesToTrackErrors.indexOf(element.tagName) != -1) {
                    var resourceUrl = element.tagName == "LINK" ? element.href : element.src;
                    var tags = {};
                    if (self.resourceErrorsNameTracking.hasOwnProperty(element.tagName.toLowerCase())) {
                        // Apply user defined filter to resource error name
                        tags.resource = self.resourceErrorsNameTracking[element.tagName.toLowerCase()](resourceUrl);
                    }
                    self.util.addItemToQueue("metrics", new self.metricsData("resource.error", "counter", 1, tags));
                    // if return it's true, we avoid that error be thrown to the console too
                    return false;
                }
            }, true);
        },
        /**
         * Track resource loading metrics
         */
        trackResourceLoading: function() {
            var self = this;
            function addResourceToQueue(value, action, tags) {
                tags.action = action;
                self.util.addItemToQueue("metrics", new self.metricsData("resource", "timer", value, tags));
            }
            setInterval(function trackResourceLoadingInterval() {
                var resourceEntries = self.util.filterResources(self.resourceLoadingTypeBlacklist, self.resourceLoadingPathFilter, telemetron.perf.getEntriesByType("resource"), self.log);
                resourceEntries.forEach(function handleResource(resource) {
                    var tags, attrs;
                    attrs = self.util.measureResource(resource);
                    // ignore resources that have no attribute timers
                    if (Object.getOwnPropertyNames(attrs).length > 0) {
                        tags = self.util.tagResource(resource, self.resourceLoadingNameTracking, self.log);
                        if (self.util.isResourceTimingComplete(attrs.domainLookup, attrs.connect, attrs.ssl, attrs.request, attrs.response)) {
                            addResourceToQueue(attrs.fetch, "fetch", tags);
                            addResourceToQueue(attrs.domainLookup, "domainLookup", tags);
                            addResourceToQueue(attrs.connect, "connect", tags);
                            addResourceToQueue(attrs.ssl, "ssl", tags);
                            addResourceToQueue(attrs.request, "request", tags);
                            addResourceToQueue(attrs.response, "response", tags);
                            addResourceToQueue(attrs.load, "load", tags);
                        } else {
                            addResourceToQueue(attrs.fetch, "fetch", tags);
                            addResourceToQueue(attrs.load, "load", tags);
                        }
                    }
                });
                self.clearResources();
            }, self.resourceLoadingTrackingInterval);
        },
        ////////////////////////////////////////
        // user timing spec methods
        /**
         * Measure a timer using the user timing specification
         * @param {string} measureName name of the measure to create
         * @returns {number}
         */
        measureTimeUserTiming: function(measureName) {
            var time;
            var measure = telemetron.perf.getEntriesByName(measureName).filter(function filterMeasures(entry) {
                return entry.entryType === "measure";
            });
            // istanbul ignore else
            if (measure.length > 0) {
                // Always use the most recent measure if more exist
                time = measure[measure.length - 1].duration;
            } else {
                this.log.debug("Measure " + measureName + " not found");
            }
            return time;
        },
        /**
         * Clear marks
         * @param {Array} marks - list of marks to clear (optional)
         */
        clearMarks: function(marks) {
            try {
                if (marks) {
                    marks.forEach(function(mark) {
                        // istanbul ignore else
                        if (mark) {
                            telemetron.perf.clearMarks(mark);
                        }
                    });
                } else {
                    telemetron.perf.clearMarks();
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },
        /**
         * Clear resources
         */
        clearResources: function() {
            try {
                telemetron.perf.clearResourceTimings();
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },
        /**
         * Clear measures
         * @param {Array} measures - list of measures to clear (optional)
         */
        clearMeasures: function(measures) {
            try {
                if (measures) {
                    measures.forEach(function(measure) {
                        telemetron.perf.clearMeasures(measure);
                    });
                } else {
                    telemetron.perf.clearMeasures();
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },
        /**
         * Register a mark using the user timing specification
         * @param markName - name of the mark to add
         */
        registerMark: function(markName) {
            try {
                // istanbul ignore else
                if (markName) {
                    telemetron.perf.mark(markName);
                } else {
                    this.log.debug("Undefined resource name to register as a mark");
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },
        /**
         * Register a measure and sends a timer using the user timing specification and metric options
         * @param {string} measureName - name of the measure to create in the browser (ie. timeto.apploaded)
         * @param {string} metricName - name of the metric to send to telemetron (ie. timeto)
         * @param {object} options - set of option (clearMarks, clearMeasures, startMark, endMark, tags and aggregations)
         */
        registerMeasure: function(measureName, metricName, options) {
            try {
                // istanbul ignore else
                if (measureName) {
                    var defaults = {
                        clearMarks: false,
                        clearMeasures: false
                    };
                    options = options || {};
                    for (var key in options) {
                        // istanbul ignore else
                        if (options.hasOwnProperty(key)) {
                            defaults[key] = options[key];
                        }
                    }
                    // Create endMark if none is set
                    if (!defaults.endMark) {
                        this.registerMark(measureName);
                        defaults.endMark = measureName;
                    }
                    telemetron.perf.measure(measureName, defaults.startMark, defaults.endMark);
                    // Measure timer
                    var time = this.measureTimeUserTiming(measureName);
                    if (time) {
                        // Push metrics to queue
                        this.util.addItemToQueue("metrics", new this.metricsData(metricName, "timer", time, defaults.tags, defaults.aggregations, defaults.aggregationFrequency));
                    } else {
                        this.log.debug("Failed to get measure time to register as timer value");
                    }
                    if (defaults.clearMarks) {
                        this.clearMarks([ defaults.startMark, defaults.endMark ]);
                    }
                    if (defaults.clearMeasures) {
                        this.clearMeasures([ measureName ]);
                    }
                } else {
                    this.log.debug("Undefined resource name to register as a measure");
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },
        ////////////////////////////////////////
        // telemetron client access methods
        /**
         * Register timer
         * @param {string} metricName - metric name to be used as metric name (Ex: timeto.firstclick)
         * @param {number} metricValue - timer value to be sent
         * @param {object} options - set of option (tags and aggregations)
         */
        registerTimer: function(metricName, metricValue, options) {
            try {
                // istanbul ignore else
                if (metricName && metricValue) {
                    options = options || {};
                    // Push metrics to queue
                    this.util.addItemToQueue("metrics", new this.metricsData(metricName, "timer", metricValue, options.tags, options.aggregations, options.aggregationFrequency));
                } else {
                    this.log.debug("Undefined metric name/value to register as a timer");
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },
        /**
         * Register counter
         * @param {string} metricName metric name to be sent (Ex: navigation.success)
         * @param {object} options - set of option (metricValue, tags and aggregations)
         */
        registerCounter: function(metricName, options) {
            try {
                this.log.debug(metricName);
                // istanbul ignore else
                if (metricName) {
                    options = options || {};
                    // Set counter default value if not defined
                    options.metricValue = options.metricValue || 1;
                    // Push metrics to queue
                    this.util.addItemToQueue("metrics", new this.metricsData(metricName, "counter", options.metricValue, options.tags, options.aggregations, options.aggregationFrequency));
                } else {
                    this.log.debug("Undefined metric name to register as a counter");
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },
        /**
         * Register gauge
         * @param {string} metricName metric name to be sent (Ex: navigation.success)
         * @param {number} metricValue gauge value to be sent
         * @param {object} options - set of option (tags and aggregations)
         */
        registerGauge: function(metricName, metricValue, options) {
            try {
                // istanbul ignore else
                if (metricName && metricValue) {
                    options = options || {};
                    // Push metrics to queue
                    this.util.addItemToQueue("metrics", new this.metricsData(metricName, "gauge", metricValue, options.tags, options.aggregations, options.aggregationFrequency));
                } else {
                    this.log.debug("Undefined metric name/value to register as a gauge");
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        }
    };
    window.telemetron = telemetron;
})(typeof window !== "undefined" ? window : undefined);

(function(window) {
    "use strict";
    function TelemetronUtil(config) {
        //Merge configs
        for (var attr in config) {
            // istanbul ignore else
            if (config.hasOwnProperty(attr)) {
                this[attr] = config[attr];
            }
        }
        this.listQueues = [];
        /**
         * Sends HTTP request to the api
         * @param {string} endpoint - action
         * @param {string} type - GET /POST
         * @param {string} requestData - request data
         */
        this.sendRequest = function(endpoint, type, requestData) {
            try {
                var self = this;
                var requestArr = [ this.apiAddress, endpoint ];
                var urlParams = type == "GET" ? requestData : null;
                urlParams ? requestArr.push(urlParams) : null;
                var requestUrl = requestArr.join("/");
                if (this.debug) {
                    console.log("Request: " + requestUrl);
                }
                var xmlHttp = new XMLHttpRequest();
                xmlHttp.open(type, requestUrl, true);
                //Send the proper header information along with the request
                if (type == "POST") {
                    xmlHttp.setRequestHeader("Content-type", "application/json");
                    xmlHttp.send(requestData);
                    //Catch response
                    xmlHttp.onreadystatechange = function() {
                        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                            if (self.debug) {
                                console.log("Post request success!");
                            }
                        }
                    };
                } else if (type == "GET") {
                    xmlHttp.send(null);
                    if (this.debug) {
                        console.log("Get request success!");
                    }
                }
            } catch (ex) {
                // istanbul ignore next
                if (this.debug) {
                    console.log(ex);
                }
            }
        };
        /**
         * Register a new queue
         * @param {string} queueName - queue name
         * @param {string} endpoint - endpoint to send requests
         * @param {int} timeInterval - interval in milliseconds, default 30000 ms
         */
        this.registerQueue = function(queueName, endpoint, timeInterval) {
            timeInterval = timeInterval || this.flushInterval;
            if (typeof queueName === "string" && typeof timeInterval === "number") {
                var self = this;
                this.listQueues[queueName] = {
                    data: [],
                    endpoint: endpoint
                };
                this.listQueues[queueName].timer = setInterval(function() {
                    var queue = self.listQueues[queueName];
                    if (!self.dryrun && queue.data.length > 0) {
                        self.sendRequest(queue.endpoint, "POST", JSON.stringify(queue.data));
                    }
                    queue.data = [];
                }, timeInterval);
                return true;
            } else {
                return false;
            }
        };
        /**
         * Unregister queue
         * @param {string} queueName - queue name
         */
        this.unregisterQueue = function(queueName) {
            if (this.listQueues[queueName]) {
                clearInterval(this.listQueues[queueName].timer);
                this.listQueues[queueName] = undefined;
                return true;
            } else {
                return false;
            }
        };
        /**
         * Sends an Item to a specific queue
         * @param {string} queueName - queue name
         * @param {object} item - object to be sent
         */
        this.addItemToQueue = function(queueName, item) {
            if (this.listQueues[queueName]) {
                this.listQueues[queueName].data.push(item);
                return true;
            } else {
                return false;
            }
        };
        /**
         * Define aggregations for a metric type
         * @param {Array} methodAggregations - list of method aggregations
         * @param {Array} globalAggregations - list of global aggregations
         * @param {Array} typeAggregations - list of type aggregations
         * @param {object} logger - logger object
         * @returns {*|Array}
         */
        this.setAggregations = function(methodAggregations, globalAggregations, typeAggregations, logger) {
            function uniq(item, index, array) {
                return item && array.indexOf(item) === index;
            }
            var aggregations = globalAggregations;
            aggregations = aggregations.concat(typeAggregations).filter(uniq);
            if (!methodAggregations) {
                aggregations = aggregations || [];
            } else {
                aggregations = aggregations.concat(methodAggregations).filter(uniq);
            }
            return this.filterAggregations(aggregations, logger);
        };
        /**
         * Define tags for a metric type
         * @param {object} methodTags - list of method tags
         * @param {object} globalTags - list of global tags
         * @param {string} typeTags - list of type tags
         * @param {string} environment - environment tag value
         * @param {string} app - app tag value
         * @returns {*}
         */
        this.setTags = function(methodTags, globalTags, typeTags, environment, app) {
            var tags = {};
            for (var key in globalTags) {
                if (globalTags.hasOwnProperty(key)) {
                    tags[key] = globalTags[key];
                }
            }
            for (key in typeTags) {
                if (typeTags.hasOwnProperty(key)) {
                    tags[key] = typeTags[key];
                }
            }
            if (!methodTags) {
                tags = tags || {};
                if (environment) {
                    tags.env = environment;
                }
            } else {
                for (key in methodTags) {
                    if (methodTags.hasOwnProperty(key)) {
                        tags[key] = methodTags[key];
                    }
                }
                if (!tags.env && environment) {
                    tags.env = environment;
                }
                if (!tags.app && app) {
                    tags.app = app;
                }
            }
            return tags;
        };
        /**
         * Define aggregation frequency
         * @param {number} methodFrequency - method aggregation frequency
         * @param {number} globalFrequency - global aggregation frequency
         * @param {number} typeFrequency - type aggregation frequency
         * @param {object} logger - logger object
         */
        this.setAggregationFrequency = function(methodFrequency, globalFrequency, typeFrequency, logger) {
            var frequency = globalFrequency;
            if (typeFrequency) {
                frequency = typeFrequency;
            }
            if (methodFrequency) {
                frequency = methodFrequency;
            }
            return this.filterAggregationFrequency(frequency, logger);
        };
        /**
         * Filter unsupported aggregations
         * @param {Array} aggregations - list of aggregations
         * @param {object} logger - logger object
         * @returns {Array}
         */
        this.filterAggregations = function(aggregations, logger) {
            var agg = [ "avg", "count", "sum", "first", "last", "p90", "p95", "min", "max", "derivative" ];
            function unsupported(item) {
                if (agg.indexOf(item) > -1) {
                    return true;
                }
                logger.debug("Discard invalid aggregation " + item);
                return false;
            }
            if (!aggregations) {
                aggregations = [];
            }
            return aggregations.filter(unsupported);
        };
        /**
         * Filter unsupported frequencies
         * @param {number} frequency - aggregation frequency
         * @param logger - logger object
         * @returns {*}
         */
        this.filterAggregationFrequency = function(frequency, logger) {
            var freq = [ 10, 30, 60, 120, 180, 300 ];
            if (freq.indexOf(frequency) > -1) {
                return frequency;
            }
            logger.debug("Discard invalid aggregation frequency " + frequency);
            return 10;
        };
        /**
         * Validates if the browser support the resource timing specification
         * @returns {boolean}
         */
        this.isResourceTimingSupported = function() {
            return !!(window.performance && window.performance.clearResourceTimings);
        };
        /**
         * Validates if the resource is considered a DNS/TCP/TLS failure
         * @param domainLookupStart {number}
         * @param connectStart {number}
         * @param requestStart {number}
         * @param responseStart {number}
         * @returns {boolean}
         */
        this.isResourceFailure = function(domainLookupStart, connectStart, requestStart, responseStart) {
            return domainLookupStart === 0 || connectStart === 0 || requestStart === 0 || responseStart === 0;
        };
        /**
         * Validate if the resource originated from a cross origin request
         * @param duration {number}
         * @param fetchStart {number}
         * @param responseEnd {number}
         * @returns {boolean}
         */
        this.isResourceCrossOrigin = function(duration, fetchStart, responseEnd) {
            return duration !== 0 && fetchStart !== 0 && responseEnd !== 0;
        };
        /**
         * Validate the resource metrics calculated are all valid
         * @param domainLookup {number}
         * @param connect {number}
         * @param ssl {number}
         * @param request {number}
         * @param response {number}
         * @returns {boolean}
         */
        this.isResourceTimingComplete = function(domainLookup, connect, ssl, request, response) {
            return domainLookup !== undefined && connect !== undefined && ssl !== undefined && request !== undefined && response !== undefined;
        };
        /**
         * Filter desired resources only
         * @param typeBlacklist
         * @param pathFilter
         * @param resources
         * @returns {*|Array.<T>}
         */
        this.filterResources = function(typeBlacklist, pathFilter, resources, logger) {
            var filteredResources;
            filteredResources = resources.filter(function filter(entry) {
                // Exclude blacklisted types
                if (typeBlacklist.indexOf(entry.initiatorType) === -1) {
                    // Apply user defined filtering function
                    try {
                        return pathFilter(entry.name);
                    } catch (ex) {
                        logger.debug(ex);
                        return false;
                    }
                } else {
                    return false;
                }
            });
            return filteredResources;
        };
        /**
         * Calculate measures based on the resource attribute timers
         * @param resource
         * @returns {{}}
         */
        this.measureResource = function(resource) {
            var attributes = {};
            /**
             * DNS/TCP/TLS failures are represented in IE/Firefox with zero values in most attributes
             * Chrome does not create a ResourceTiming entry for failures
             */
            if (this.isResourceFailure(resource.domainLookupStart, resource.connectStart, resource.requestStart, resource.responseStart)) {
                if (this.isResourceCrossOrigin(resource.duration, resource.fetchStart, resource.responseEnd)) {
                    attributes.fetch = resource.responseEnd - resource.fetchStart;
                    attributes.load = resource.duration;
                }
            } else {
                attributes.fetch = resource.responseEnd - resource.fetchStart;
                attributes.domainLookup = resource.domainLookupEnd - resource.domainLookupStart;
                attributes.connect = resource.connectEnd - resource.connectStart;
                attributes.ssl = resource.secureConnectionStart ? resource.connectEnd - resource.secureConnectionStart : 0;
                attributes.request = resource.responseStart - resource.requestStart;
                attributes.response = resource.responseEnd - resource.responseStart;
                attributes.load = resource.duration;
            }
            return attributes;
        };
        /**
         * Builds a tags object based on the resource attributes
         * @param resource
         * @param trackResourceName
         * @param logger
         * @returns {{}}
         */
        this.tagResource = function(resource, trackResourceName, logger) {
            var tags = {};
            try {
                tags.initiator = resource.initiatorType;
                if (trackResourceName.hasOwnProperty(tags.initiator)) {
                    tags.resource = trackResourceName[tags.initiator](resource.name);
                }
            } catch (ex) {
                logger.debug(ex);
            }
            return tags;
        };
    }
    window.TelemetronUtil = TelemetronUtil;
})(window);