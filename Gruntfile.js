/*jshint strict:false */

module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            gruntfile: {
                src: ['Gruntfile.js']
            },
            js: {
                src: ['src/**/*.js', '!src/vendor/*.js']

            }
        },

        uglify: {
            dist: {
                options: {
                    mangle: true,
                    banner: grunt.file.readJSON('package.json').banner
                },
                files: {
                    'dist/telemetron.client.min.js': ['src/*.js'],
                    'dist/telemetron.exceptions.angular.min.js': ['src/exception-handlers/angularjs/*.js']
                }
            },
            debug: {
                options: {
                    mangle: false,
                    beautify: true,
                    compress: false,
                    preserveComments: true,
                    banner: grunt.file.readJSON('package.json').banner
                },
                files: {
                    'dist/telemetron.client.js': ['src/*.js'],
                    'dist/telemetron.exceptions.angular.js': ['src/exception-handlers/angularjs/*.js']
                }
            }
        },

        karma: {
            unit: {
                configFile: 'karma.conf.js',
                singleRun: true
            }
        },

        watch: {
            scripts: {
                files: ['src/**/*.js', 'tests/*.js', './*.js'],
                tasks: ['test']
            }
        },

        versioncheck: {
            options: {
                hideUpToDate : true
            }
        },
        bump: {
            options: {
                files: ['package.json', 'bower.json', '.jenkins/main.yml'],
                updateConfigs: [],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: ['package.json', 'bower.json', '.jenkins/main.yml', 'dist/telemetron.client.js', 'dist/telemetron.client.min.js'],
                createTag: true,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: true,
                pushTo: 'origin',
                gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
                globalReplace: false,
                prereleaseName: false,
                regExp: false
            }
        }
    });

    grunt.registerTask('dev', [
        'watch'
    ]);

    grunt.registerTask('test', [
        'jshint',
        'karma'
    ]);

    grunt.registerTask('default', [
        'jshint',
        'karma',
        'uglify'
    ]);

    grunt.registerTask('release', [
        'default',
        'bump'
    ]);
};
