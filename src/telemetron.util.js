(function (window) {
'use strict';

    function TelemetronUtil(config) {
        //Merge configs
        for (var attr in config) {
            // istanbul ignore else
            if (config.hasOwnProperty(attr)) {
                this[attr] = config[attr];
            }
        }

        this.listQueues = [];

        /**
         * Sends HTTP request to the api
         * @param {string} endpoint - action
         * @param {string} type - GET /POST
         * @param {string} requestData - request data
         */
        this.sendRequest = function (endpoint, type, requestData) {
            try {
                var self = this;
                var requestArr = [this.apiAddress, endpoint];
                var urlParams = type == 'GET' ? requestData : null;
                urlParams ? requestArr.push(urlParams) : null;
                var requestUrl = requestArr.join('/');

                if (this.debug) {
                    console.log('Request: ' + requestUrl);
                }

                var xmlHttp = new XMLHttpRequest();

                xmlHttp.open(type, requestUrl, true);

                //Send the proper header information along with the request
                if (type == 'POST') {
                    xmlHttp.setRequestHeader('Content-type', 'application/json');
                    xmlHttp.send(requestData);

                    //Catch response
                    xmlHttp.onreadystatechange = function () {
                        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                            if (self.debug) {
                                console.log('Post request success!');
                            }
                        }
                    };
                }
                else if (type == 'GET') {
                    xmlHttp.send(null);
                    if (this.debug) {
                        console.log('Get request success!');
                    }
                }
            }
            catch (ex) {
                // istanbul ignore next
                if (this.debug) {
                    console.log(ex);
                }
            }
        };

        /**
         * Register a new queue
         * @param {string} queueName - queue name
         * @param {string} endpoint - endpoint to send requests
         * @param {int} timeInterval - interval in milliseconds, default 30000 ms
         */
        this.registerQueue = function(queueName, endpoint, timeInterval) {
            timeInterval = timeInterval || this.flushInterval;

            if (typeof queueName === 'string' && typeof timeInterval === 'number') {
                var self = this;

                this.listQueues[queueName] = {
                    data: [],
                    endpoint: endpoint
                };

                this.listQueues[queueName].timer = setInterval(function() {
                    var queue = self.listQueues[queueName];

                    if (!self.dryrun && queue.data.length > 0) {
                        self.sendRequest(queue.endpoint, 'POST', JSON.stringify(queue.data));
                    }

                    queue.data = [];
                }, timeInterval);

                return true;
            } else {
                return false;
            }
        };

        /**
         * Unregister queue
         * @param {string} queueName - queue name
         */
        this.unregisterQueue = function (queueName) {
            if (this.listQueues[queueName]) {
                clearInterval(this.listQueues[queueName].timer);
                this.listQueues[queueName] = undefined;
                return true;
            } else {
                return false;
            }
        };

        /**
         * Sends an Item to a specific queue
         * @param {string} queueName - queue name
         * @param {object} item - object to be sent
         */
        this.addItemToQueue = function (queueName, item) {
            if (this.listQueues[queueName]) {
                this.listQueues[queueName].data.push(item);
                return true;
            } else {
                return false;
            }
        };

        /**
         * Define aggregations for a metric type
         * @param {Array} methodAggregations - list of method aggregations
         * @param {Array} globalAggregations - list of global aggregations
         * @param {Array} typeAggregations - list of type aggregations
         * @param {object} logger - logger object
         * @returns {*|Array}
         */
        this.setAggregations = function (methodAggregations, globalAggregations, typeAggregations, logger) {
            function uniq(item, index, array) {
                return item && array.indexOf(item) === index;
            }

            var aggregations = globalAggregations;

            aggregations = aggregations.concat(typeAggregations).filter(uniq);

            if (!methodAggregations) {
                aggregations = aggregations || [];
            } else {
                aggregations = aggregations.concat(methodAggregations).filter(uniq);
            }

            return this.filterAggregations(aggregations, logger);
        };

        /**
         * Define tags for a metric type
         * @param {object} methodTags - list of method tags
         * @param {object} globalTags - list of global tags
         * @param {string} typeTags - list of type tags
         * @param {string} environment - environment tag value
         * @param {string} app - app tag value
         * @returns {*}
         */
        this.setTags = function (methodTags, globalTags, typeTags, environment, app) {
            var tags = {};

            for (var key in globalTags) {
                if (globalTags.hasOwnProperty(key)) {
                    tags[key] = globalTags[key];
                }
            }

            for (key in typeTags) {
                if (typeTags.hasOwnProperty(key)) {
                    tags[key] = typeTags[key];
                }
            }

            if (!methodTags) {
                tags = tags || {};

                if (environment) {
                    tags.env = environment;
                }
            } else {
                for (key in methodTags) {
                    if (methodTags.hasOwnProperty(key)) {
                        tags[key] = methodTags[key];
                    }
                }

                if (!tags.env && environment) {
                    tags.env = environment;
                }

                if (!tags.app && app) {
                    tags.app = app;
                }
           }

            return tags;
        };

        /**
         * Define aggregation frequency
         * @param {number} methodFrequency - method aggregation frequency
         * @param {number} globalFrequency - global aggregation frequency
         * @param {number} typeFrequency - type aggregation frequency
         * @param {object} logger - logger object
         */
        this.setAggregationFrequency = function(methodFrequency, globalFrequency, typeFrequency, logger) {
            var frequency = globalFrequency;

            if (typeFrequency) {
                frequency = typeFrequency;
            }

            if (methodFrequency) {
                frequency = methodFrequency;
            }

            return this.filterAggregationFrequency(frequency, logger);
        };

        /**
         * Filter unsupported aggregations
         * @param {Array} aggregations - list of aggregations
         * @param {object} logger - logger object
         * @returns {Array}
         */
        this.filterAggregations = function (aggregations, logger) {
            var agg = ['avg', 'count', 'sum', 'first', 'last', 'p90', 'p95', 'min', 'max', 'derivative'];

            function unsupported(item) {
                if (agg.indexOf(item) > -1) {
                    return true;
                }

                logger.debug('Discard invalid aggregation ' + item);
                return false;
            }

            if (!aggregations) {
                aggregations = [];
            }

            return aggregations.filter(unsupported);
        };

        /**
         * Filter unsupported frequencies
         * @param {number} frequency - aggregation frequency
         * @param logger - logger object
         * @returns {*}
         */
        this.filterAggregationFrequency = function (frequency, logger) {
            var freq = [10, 30, 60 ,120, 180, 300];

            if (freq.indexOf(frequency) > -1) {
                return frequency;
            }

            logger.debug('Discard invalid aggregation frequency ' + frequency);
            return 10;
        };

        /**
         * Validates if the browser support the resource timing specification
         * @returns {boolean}
         */
        this.isResourceTimingSupported = function () {
            return !!(window.performance && window.performance.clearResourceTimings);
        };

        /**
         * Validates if the resource is considered a DNS/TCP/TLS failure
         * @param domainLookupStart {number}
         * @param connectStart {number}
         * @param requestStart {number}
         * @param responseStart {number}
         * @returns {boolean}
         */
        this.isResourceFailure = function (domainLookupStart, connectStart, requestStart, responseStart) {
            return domainLookupStart === 0 || connectStart === 0 || requestStart === 0 || responseStart === 0;
        };

        /**
         * Validate if the resource originated from a cross origin request
         * @param duration {number}
         * @param fetchStart {number}
         * @param responseEnd {number}
         * @returns {boolean}
         */
        this.isResourceCrossOrigin = function (duration, fetchStart, responseEnd) {
            return duration !== 0 && fetchStart !== 0 && responseEnd !== 0;
        };

        /**
         * Validate the resource metrics calculated are all valid
         * @param domainLookup {number}
         * @param connect {number}
         * @param ssl {number}
         * @param request {number}
         * @param response {number}
         * @returns {boolean}
         */
        this.isResourceTimingComplete = function (domainLookup, connect, ssl, request, response) {
            return domainLookup !== undefined && connect !== undefined && ssl !== undefined && request !== undefined && response !== undefined;
        };

        /**
         * Filter desired resources only
         * @param typeBlacklist
         * @param pathFilter
         * @param resources
         * @returns {*|Array.<T>}
         */
        this.filterResources = function (typeBlacklist, pathFilter, resources, logger) {
            var filteredResources;

            filteredResources = resources.filter(function filter(entry) {
                // Exclude blacklisted types
                if (typeBlacklist.indexOf(entry.initiatorType) === -1) {
                    // Apply user defined filtering function
                    try {
                        return pathFilter(entry.name);
                    } catch (ex) {
                        logger.debug(ex);
                        return false;
                    }
                } else {
                    return false;
                }
            });

            return filteredResources;
        };

        /**
         * Calculate measures based on the resource attribute timers
         * @param resource
         * @returns {{}}
         */
        this.measureResource = function (resource) {
            var attributes = {};

            /**
             * DNS/TCP/TLS failures are represented in IE/Firefox with zero values in most attributes
             * Chrome does not create a ResourceTiming entry for failures
             */
            if (this.isResourceFailure(resource.domainLookupStart, resource.connectStart, resource.requestStart, resource.responseStart)) {
                if (this.isResourceCrossOrigin(resource.duration, resource.fetchStart, resource.responseEnd)) {
                    attributes.fetch = resource.responseEnd - resource.fetchStart;
                    attributes.load = resource.duration;
                }
            } else {
                attributes.fetch = resource.responseEnd - resource.fetchStart;
                attributes.domainLookup = resource.domainLookupEnd - resource.domainLookupStart;
                attributes.connect = resource.connectEnd - resource.connectStart;
                attributes.ssl = resource.secureConnectionStart ? (resource.connectEnd - resource.secureConnectionStart) : 0;
                attributes.request = resource.responseStart - resource.requestStart;
                attributes.response = resource.responseEnd - resource.responseStart;
                attributes.load = resource.duration;
            }

            return attributes;
        };

        /**
         * Builds a tags object based on the resource attributes
         * @param resource
         * @param trackResourceName
         * @param logger
         * @returns {{}}
         */
        this.tagResource = function (resource, trackResourceName, logger) {
            var tags = {};

            try {
                tags.initiator = resource.initiatorType;

                if (trackResourceName.hasOwnProperty(tags.initiator)) {
                    tags.resource = trackResourceName[tags.initiator](resource.name);
                }
            } catch (ex) {
                logger.debug(ex);
            }

            return tags;
        };
    }

    window.TelemetronUtil = TelemetronUtil;

})(window);
