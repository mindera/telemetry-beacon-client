// Include in index.html so that app level exceptions are handled.
(function() {
    'use strict';

    angular
        .module('telemetron.exceptions.angular', [])
        .provider('telemetronExceptionHandler', telemetronExceptionHandlerProvider)
        .config(config);

    /**
     * Must configure the exception handling
     */
    function telemetronExceptionHandlerProvider() {
        /* jshint validthis:true */
        this.config = {
            beacon: undefined
        };

        this.configure = function (beacon) {
            this.config.beacon = beacon;
        };

        this.$get = function() {
            return {config: this.config};
        };
    }

    config.$inject = ['$provide'];

    /**
     * Configure by setting a beacon client.
     * Accessible via config.beacon (via config value).
     * @param  {Object} $provide
     */
    /* @ngInject */
    function config($provide) {
        $provide.decorator('$exceptionHandler', extendTelemetronExceptionHandler);
    }

    extendTelemetronExceptionHandler.$inject = ['$delegate', 'telemetronExceptionHandler', '$injector'];

    /**
     * Extend the $exceptionHandler service to send metrics with telemetron beacon.
     * @param  {Object} $delegate
     * @param  {Object} telemetronExceptionHandler
     * @return {Function} the decorated $exceptionHandler service
     */
    function extendTelemetronExceptionHandler($delegate, telemetronExceptionHandler, $injector) {
        return function(exception, cause) {
            var location = $injector.get('$location');
            var errorData = {
                exception: exception,
                cause: cause,
                url: location.path()
            };
            $delegate(exception, cause);
            // Beacon counter register
            if (telemetronExceptionHandler.config.beacon) {
                telemetronExceptionHandler.config.beacon.registerCounter('error', {
                    tags: {
                        url: errorData.url
                    }
                });
            }
        };
    }
})();
