(function(window) {
    'use strict';

    var logger = {
        enabled: false,

        initialize: function (toggle) {
            if (toggle) {
                this.enabled = true;

                // istanbul ignore next
                this.debug = function (msg) {
                    console.log(msg);
                };
            }
        },

        debug: function(msg) {
            // noop
        }
    };

    /**
     * Dummy default resource filter
     * @param name
     * @returns {boolean}
     */
    var defaultFilter = function (name) {
        return true;
    };

    /**
     * @description
     *
     * This object provides a client for the Telemetron service to register
     * application metrics. It can be called as follows:
     *
     * telemetron.initialize({
     *  debug: false,
     *  environment: 'local',
     *  namespace: 'mobile'
     *  registerFirstClick : true,
     *  firstClickMetricName: 'timeto',
     *  firstClickMetricTags: {mark: 'firstclick'},
     *  });
     * telemetron.registerMeasure('your.measure.name', 'your.metric.name');
     *
     */
    var telemetron = {
        // telemetron details
        apiAddress: '//beacon.telemetron.io',
        endpoints: {
            metrics: 'beacon/metrics'
        },
        // console log accessor
        log: logger,
        // firstClick event helper
        firstClickFired: false,
        // window performance api accessor
        perf: window.performance,
        defaults: {
            // enable dryrun mode - ie. do not actually flush metrics out
            dryrun: false,
            // enable debug log messages
            debug: false,
            // environment to be used as tag - ie. development, production, etc.
            environment: undefined,
            // namespace to be used as metrics prefix - ie web, application, mobile, etc.
            namespace: 'web',
            // global tags
            tags: {},
            // global aggregations
            aggregations: [],
            // aggregation frequency
            aggregationFrequency: 10,
            // metric type defaults
            timer: {
                tags: {},
                aggregations: ['avg', 'p90', 'count']
            },
            counter: {
                tags: {},
                aggregations: ['sum', 'count']
            },
            gauge: {
                tags: {},
                aggregations: ['sum', 'count']
            },
            other: {
                tags: {},
                aggregations: ['last'],
                aggregationFrequency: 10
            },
            // enable first click tracking
            registerFirstClick: true,
            // enable resource error tracking
            registerResourceErrors: false,
            // track resource names on resource error tracking
            resourceErrorsNameTracking: {},
            // resource error type blacklist
            resourceErrorsTypeBlacklist: [],
            // enable resource loading tracking
            registerResourceLoading: true,
            // resource loading tracking interval
            resourceLoadingTrackingInterval: 5000,
            // resource type blacklist
            resourceLoadingTypeBlacklist: [],
            // resource path whitelist
            resourceLoadingPathFilter: defaultFilter,
            // track resource names
            resourceLoadingNameTracking: {},
            firstClickMeasureName: 'firstclick',
            firstClickMetricName: 'timeto',
            firstClickMetricTags: {mark: 'firstclick'},
            appLoadedMeasureName: 'apploaded',
            appLoadedMetricName: 'timeto',
            appLoadedMetricTags: {mark: 'apploaded'},
            // metrics flush interval to telemetron
            flushInterval: 30000
        },

        /**
         * Initialize the Telemetron client settings and register events
         */
        initialize: function(settings) {
            var self = this;

            this.defaults.debug = (settings && settings.debug) ? settings.debug : this.defaults.debug;
            this.log.initialize(this.defaults.debug);

            try {
                settings = typeof settings !== 'undefined' ? settings : null;

                // Set default properties
                for (var key in self.defaults) {
                    if (self.defaults.hasOwnProperty(key)) {
                        self[key] = self.defaults[key];
                    }
                }

                // Set custom properties
                if (settings) {
                    for (key in settings) {
                        if (settings.hasOwnProperty(key)) {
                            self[key] = settings[key];
                        }
                    }
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }

            // Set first click event
            if (self.registerFirstClick) {
                self.firstClick();
            }

            self.util = new TelemetronUtil({
                debug : this.debug,
                apiAddress: this.apiAddress,
                dryrun: this.dryrun,
                flushInterval: this.flushInterval
            });

            self.util.registerQueue('metrics', this.endpoints.metrics,  this.flushInterval);

            if (self.registerResourceErrors) {
                self.trackResourceErrors();
            }

            if (self.registerResourceLoading && self.util.isResourceTimingSupported()) {
                self.trackResourceLoading();
            }

            // Metrics data object
            self.metricsData = function(name, type, value, tags, aggregations, aggregationFrequency) {
                return {
                    name: name,
                    type: type,
                    value: value,
                    tags: self.util.setTags(tags, self.tags, self[type].tags, self.environment, self.app),
                    aggregations:
                        self.util.setAggregations(aggregations, self.aggregations, self[type].aggregations, self.log),
                    aggregationFrequency:
                        self.util.setAggregationFrequency(aggregationFrequency, self.aggregationFrequency, self[type].aggregationFrequency, self.log),
                    namespace: self.namespace
                };
            };
        },

        /**
         * Register metric/mark when app is loaded
         */
        appLoaded: function () {
            this.registerMeasure(this.appLoadedMeasureName, this.appLoadedMetricName, {
                tags: this.appLoadedMetricTags,
                aggregations: []
            });
        },

        /**
         * Adds event listener to window on click
         * to register metric for first click
         */
        firstClick: function () {
            // istanbul ignore else
            if (window.addEventListener) {
                window.addEventListener('click', this);
            } else {
                this.log.debug('Click listener was not registered with success.');
            }
        },

        /**
         * Generic handler for events
         */
        handleEvent: function (event) {
            switch (event.type) {
                case 'click':
                    this.handleFirstClick();
                    break;
            }
        },

        /**
         * Handle first click
         */
        handleFirstClick: function () {
            if (!this.firstClickFired) {
                this.registerMeasure(this.firstClickMeasureName, this.firstClickMetricName,  {
                    tags: this.firstClickMetricTags,
                    aggregations: []
                });
                this.firstClickFired = true;
            }
        },

        /**
         * Track resource load error metrics
         */
        trackResourceErrors: function () {
            var self = this;

            // Apply user defined black list filter on resources to track errors
            var allElementsToCatch = ['IMG', 'SCRIPT', 'LINK'];
            var resourceErrorsTypeBlacklistUpperCase = self.resourceErrorsTypeBlacklist.map(function(value) {
                return value.toUpperCase();
            });
            var resourceEntriesToTrackErrors = allElementsToCatch.filter(function filter(entry) {
                // Exclude blacklisted types
                return resourceErrorsTypeBlacklistUpperCase.indexOf(entry) === -1;
            });


            window.addEventListener('error', function (event) {
                var element = event.target;
                if(resourceEntriesToTrackErrors.indexOf(element.tagName) != -1){
                    var resourceUrl = element.tagName == 'LINK' ? element.href: element.src;
                    var tags = {};
                    if (self.resourceErrorsNameTracking.hasOwnProperty(element.tagName.toLowerCase())) {
                        // Apply user defined filter to resource error name
                        tags.resource = self.resourceErrorsNameTracking[element.tagName.toLowerCase()](resourceUrl);
                    }

                    self.util.addItemToQueue('metrics', new self.metricsData('resource.error', 'counter', 1, tags));

                    // if return it's true, we avoid that error be thrown to the console too
                    return false;
                }
            }, true);
        },

        /**
         * Track resource loading metrics
         */
        trackResourceLoading: function () {
            var self = this;

            function addResourceToQueue(value, action, tags) {
                tags.action = action;
                self.util.addItemToQueue('metrics', new self.metricsData('resource', 'timer', value, tags));
            }

            setInterval(function trackResourceLoadingInterval() {
                var resourceEntries = self.util.filterResources(self.resourceLoadingTypeBlacklist,
                    self.resourceLoadingPathFilter, telemetron.perf.getEntriesByType('resource'), self.log);

                resourceEntries.forEach(function handleResource(resource) {
                    var tags, attrs;
                    attrs = self.util.measureResource(resource);

                    // ignore resources that have no attribute timers
                    if (Object.getOwnPropertyNames(attrs).length > 0) {
                        tags = self.util.tagResource(resource, self.resourceLoadingNameTracking, self.log);

                        if (self.util.isResourceTimingComplete(attrs.domainLookup, attrs.connect, attrs.ssl, attrs.request, attrs.response)) {
                            addResourceToQueue(attrs.fetch, 'fetch', tags);
                            addResourceToQueue(attrs.domainLookup, 'domainLookup', tags);
                            addResourceToQueue(attrs.connect, 'connect', tags);
                            addResourceToQueue(attrs.ssl, 'ssl', tags);
                            addResourceToQueue(attrs.request, 'request', tags);
                            addResourceToQueue(attrs.response, 'response', tags);
                            addResourceToQueue(attrs.load, 'load', tags);
                        } else {
                            addResourceToQueue(attrs.fetch, 'fetch', tags);
                            addResourceToQueue(attrs.load, 'load', tags);
                        }
                    }
                });

                self.clearResources();
            }, self.resourceLoadingTrackingInterval);
        },

        ////////////////////////////////////////
        // user timing spec methods

        /**
         * Measure a timer using the user timing specification
         * @param {string} measureName name of the measure to create
         * @returns {number}
         */
        measureTimeUserTiming: function (measureName) {
            var time;
            var measure = telemetron.perf.getEntriesByName(measureName).filter(function filterMeasures(entry) {
                return entry.entryType === 'measure';
            });

            // istanbul ignore else
            if (measure.length > 0) {
                // Always use the most recent measure if more exist
                time = measure[measure.length - 1].duration;
            } else {
                this.log.debug('Measure ' + measureName + ' not found');
            }

            return time;
        },

        /**
         * Clear marks
         * @param {Array} marks - list of marks to clear (optional)
         */
        clearMarks: function (marks) {
            try {
                if (marks) {
                    marks.forEach(function (mark) {
                        // istanbul ignore else
                        if (mark) {
                            telemetron.perf.clearMarks(mark);
                        }
                    });
                } else {
                    telemetron.perf.clearMarks();
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },

        /**
         * Clear resources
         */
        clearResources: function () {
            try {
                telemetron.perf.clearResourceTimings();
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },

        /**
         * Clear measures
         * @param {Array} measures - list of measures to clear (optional)
         */
        clearMeasures: function (measures) {
            try {
                if (measures) {
                    measures.forEach(function (measure) {
                        telemetron.perf.clearMeasures(measure);
                    });
                } else {
                    telemetron.perf.clearMeasures();
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },

        /**
         * Register a mark using the user timing specification
         * @param markName - name of the mark to add
         */
        registerMark: function (markName) {
            try {
                // istanbul ignore else
                if (markName) {
                    telemetron.perf.mark(markName);
                } else {
                    this.log.debug('Undefined resource name to register as a mark');
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },

        /**
         * Register a measure and sends a timer using the user timing specification and metric options
         * @param {string} measureName - name of the measure to create in the browser (ie. timeto.apploaded)
         * @param {string} metricName - name of the metric to send to telemetron (ie. timeto)
         * @param {object} options - set of option (clearMarks, clearMeasures, startMark, endMark, tags and aggregations)
         */
        registerMeasure: function (measureName, metricName, options) {
            try {
                // istanbul ignore else
                if (measureName) {
                    var defaults = {
                        clearMarks: false,
                        clearMeasures: false
                    };

                    options = options || {};

                    for (var key in options) {
                        // istanbul ignore else
                        if (options.hasOwnProperty(key)) {
                            defaults[key] = options[key];
                        }
                    }

                    // Create endMark if none is set
                    if (!defaults.endMark) {
                        this.registerMark(measureName);
                        defaults.endMark = measureName;
                    }

                    telemetron.perf.measure(measureName, defaults.startMark, defaults.endMark);

                    // Measure timer
                    var time = this.measureTimeUserTiming(measureName);

                    if (time) {
                        // Push metrics to queue
                        this.util.addItemToQueue('metrics', new this.metricsData(metricName, 'timer', time,
                            defaults.tags, defaults.aggregations, defaults.aggregationFrequency));
                    } else {
                        this.log.debug('Failed to get measure time to register as timer value');
                    }

                    if (defaults.clearMarks) {
                        this.clearMarks([defaults.startMark, defaults.endMark]);
                    }

                    if (defaults.clearMeasures) {
                        this.clearMeasures([measureName]);
                    }
                } else {
                    this.log.debug('Undefined resource name to register as a measure');
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },

        ////////////////////////////////////////
        // telemetron client access methods

        /**
         * Register timer
         * @param {string} metricName - metric name to be used as metric name (Ex: timeto.firstclick)
         * @param {number} metricValue - timer value to be sent
         * @param {object} options - set of option (tags and aggregations)
         */
        registerTimer: function (metricName, metricValue, options) {
            try {
                // istanbul ignore else
                if (metricName && metricValue) {
                    options = options || {};

                    // Push metrics to queue
                    this.util.addItemToQueue('metrics', new this.metricsData(metricName, 'timer', metricValue,
                        options.tags, options.aggregations, options.aggregationFrequency));
                } else {
                    this.log.debug('Undefined metric name/value to register as a timer');
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },

        /**
         * Register counter
         * @param {string} metricName metric name to be sent (Ex: navigation.success)
         * @param {object} options - set of option (metricValue, tags and aggregations)
         */
        registerCounter: function (metricName, options) {
            try {
                this.log.debug(metricName);

                // istanbul ignore else
                if (metricName) {
                    options = options || {};

                    // Set counter default value if not defined
                    options.metricValue = options.metricValue || 1;

                    // Push metrics to queue
                    this.util.addItemToQueue('metrics', new this.metricsData(metricName, 'counter', options.metricValue,
                        options.tags, options.aggregations, options.aggregationFrequency));
                } else {
                    this.log.debug('Undefined metric name to register as a counter');
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        },

        /**
         * Register gauge
         * @param {string} metricName metric name to be sent (Ex: navigation.success)
         * @param {number} metricValue gauge value to be sent
         * @param {object} options - set of option (tags and aggregations)
         */
        registerGauge: function (metricName, metricValue, options) {
            try {
                // istanbul ignore else
                if (metricName && metricValue) {
                    options = options || {};

                    // Push metrics to queue
                    this.util.addItemToQueue('metrics', new this.metricsData(metricName, 'gauge', metricValue,
                        options.tags, options.aggregations, options.aggregationFrequency));
                } else {
                    this.log.debug('Undefined metric name/value to register as a gauge');
                }
            } catch (ex) {
                // istanbul ignore next
                this.log.debug(ex);
            }
        }
    };

    window.telemetron = telemetron;
}(typeof window !== 'undefined' ? window : undefined));
